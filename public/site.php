<!doctype html>

<html lang="ru">

  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
    <title>WEB</title>
  </head>

<div class="form-wrap">
  <form target="_blank" action="" method="POST">
              <br/>Имя:<br/>
              <input type="text" name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"><br/>
              <br/>email:<br/>
              <input type="email" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br/>
              <br/>Дата рождения:<br/>
                <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="text"/>
              <br/>
                <br/><a id="gender"></a>Пол:<br/>
                  <input type="radio" name="gender" value="male" <?php if ($values['gender'] == 'male') {print 'checked="checked"';} ?>/>Мужской<br/>
                  <input type="radio" name="gender" value="female" <?php if ($values['gender'] == 'female') {print 'checked="checked"';} ?>/>Женский<br/>
              <br/>Кол-во конечностей:<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="1" <?php if ($values['limb'] == '1') {print 'checked="checked"';} ?> />1<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="2" <?php if ($values['limb'] == '2') {print 'checked="checked"';} ?> />2<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="3" <?php if ($values['limb'] == '3') {print 'checked="checked"';} ?> />3<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="4" <?php if ($values['limb'] == '4') {print 'checked="checked"';} ?> />4<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value=">4" <?php if ($values['limb'] == '>4') {print 'checked="checked"';} ?> />>4<br/>    
              <br/>Сверхспособности:<br/>
                <input type="checkbox" name="super1" value="бессмертие" <?php if ($values['super1'] != '') {print 'checked="checked"';} ?> />Бессмертие<br/>
                <input type="checkbox" name="super2" value="прохождение сквозь стены" <?php if ($values['super2'] != '') {print 'checked="checked"';} ?> />Прохождение сквозь стены<br/>
                <input type="checkbox" name="super3" value="левитация" <?php if ($values['super3'] != '') {print 'checked="checked"';} ?> />Левитация<br/>
                <input type="checkbox" name="super4" value="повышенная стипуха" <?php if ($values['super4'] != '') {print 'checked="checked"';} ?> />повышенная стипуха<br/>
              <br/>Биография:<br/>
                <textarea name="message" value="<?php print $values['message']; ?>">Расскажите о себе</textarea>
              <br/>
                <input type="checkbox" name="check" value="+" <?php if ($values['check'] != '') {print 'checked="checked"';} ?>/>С контрактом ознакомлен<br/>
              <input type="submit" name="send" value="Отправить" class="submit"/>        
            </form>
</div>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $mess) {
	print($mess);
  }
  print('</div><br/><br/>');
}
?>
</html>